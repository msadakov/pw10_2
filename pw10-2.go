package main

import (
	"fmt"
	"math"
)

var (
	x, ln, b, c, d, g, av1, av2 float64
)

func main() {
	fmt.Println("  #      ln         av1        av2         g")
	x = 0.5
	for i := 1; i <= 20; i++ {
		ln = math.Log(1 + x)
		// Дополнительные переменные
		b = 1 - (x / 2)
		c = 1/4 + (x / 5)
		d = 1/3 - x*c
		// Упрощённое уравнение
		av1 = x*b + x*x*x*d*c

		av2 = x*(1-(x/2)) + x*x*x*(1/3-x*(0.25+x/5))

		g = math.Abs(ln - av1)

		fmt.Printf("%3d %10.6f %10.6f %10.6f %10.6f\n", i, ln, av1, av2, g)
		x = 1 - ((float64(i) + 1) / (float64(i) + 2))
	}
}
